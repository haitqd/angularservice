import { Component, OnInit, } from '@angular/core';
import { CustomerService } from '../customer.service';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  firstName:string="";
  lastName:string="";
  public customers: any[];
  constructor(public cusService: CustomerService) { }
  addCustomer() {
    var newCustomer = {
      FirstName: this.firstName,
      LastName: this.lastName,
    };
    
    this.customers.push(newCustomer);
  }
  ngOnInit() {
    this.customers = this.cusService.GetList();
  }

}
